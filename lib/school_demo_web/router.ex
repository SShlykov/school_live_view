defmodule SchoolDemoWeb.Router do
  use SchoolDemoWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {SchoolDemoWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", SchoolDemoWeb do
    pipe_through :browser

    live "/", PageLive, :index
    live "/slider", SliderLive, :index
    live "/live_serarch", SearcherLive, :index
    live "/shopping", ShoppingLive, :index
  end

  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: SchoolDemoWeb.Telemetry
    end
  end
end
