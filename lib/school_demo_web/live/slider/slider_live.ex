defmodule SchoolDemoWeb.SliderLive do
  use SchoolDemoWeb, :live_view

  def mount(_params, _session, socket) do
    socket = socket
      |> assign(:max_price_products, 150)
      |> assign(:min_price_products, 40)
      |> assign(:max_price_customer, 100)
      |> assign(:min_price_customer, 60)
    {:ok, socket}
  end

  def handle_event("max-tomin", _some, %{assigns: %{min_price_products: min_price_p, min_price_customer: min_price_c}} = socket) do
    case min_price_p > min_price_c do
      true ->
        {:noreply, assign(socket, :max_price_customer, min_price_p)}
      false ->
        {:noreply, assign(socket, :max_price_customer, min_price_c)}
    end
  end

  def handle_event("max-tomax", _some, %{assigns: %{max_price_products: max_price_p, max_price_customer: max_price_c}} = socket) do
    case max_price_p > max_price_c do
      true ->
        {:noreply, assign(socket, :max_price_customer, max_price_p)}
      false ->
        {:noreply, assign(socket, :max_price_customer, max_price_c)}
    end
  end

  def handle_event("min-tomin", _some, %{assigns: %{min_price_products: min_price_p, min_price_customer: min_price_c}} = socket) do
    case min_price_p < min_price_c do
      true ->
        {:noreply, assign(socket, :min_price_customer, min_price_p)}
      false ->
        {:noreply, assign(socket, :min_price_customer, min_price_c)}
    end
  end

  def handle_event("min-tomax", _some, %{assigns: %{max_price_products: max_price_p, max_price_customer: max_price_c}} = socket) do
    case max_price_p < max_price_c do
      true ->
        {:noreply, assign(socket, :min_price_customer, max_price_p)}
      false ->
        {:noreply, assign(socket, :min_price_customer, max_price_c)}
    end
  end

  def handle_event("change-min", %{"min_price_customer" => price}, socket) do
    {:noreply, assign(socket, :min_price_customer, String.to_integer(price))}
  end

  def handle_event("change-max", %{"max_price_customer" => price}, socket) do
    {:noreply, assign(socket, :max_price_customer, String.to_integer(price))}
  end

  def render(assigns) do
    ~L"""
    <h1 class="font-bold pb-10 text-2xl">Price slider</h1>
    <div id="light">
      <p class="left-line-slug"><%= @min_price_products %></p>
      <div class="meter">
        <span style="
          margin-left: <%= calc_margin(@max_price_products, @min_price_products, @min_price_customer) %>%;
          width: <%= calc_width(@max_price_products, @min_price_products, @max_price_customer, @min_price_customer) %>%">
        <%= @min_price_customer %> - <%= @max_price_customer %> ₽
        </span>
      </div>
      <p class="right-line-slug"><%= @max_price_products %></p>
    </div>

    <div class="py-4">
      Minimal prices:
      <br>
      <button phx-click="min-tomin" class="btn-blue">
      min
      </button>
      <button phx-click="min-tomax" class="btn-blue">
      max
      </button>
      <form phx-change="change-min" class="inline-block">
        <label class="std-label">
          <input class="std-input" id="min_price_customer" type="number" name="min_price_customer" value="<%= @min_price_customer %>">
          ₽
        </label>
      </form>
    </div>

    <div class="py-4">
      Maximal prices:
      <br>
      <button phx-click="max-tomin" class="btn-blue">
      min
      </button>
      <button phx-click="max-tomax" class="btn-blue">
      max
      </button>
      <form phx-change="change-max" class="inline-block">
        <label class="std-label">
          <input class="std-input" id="max_price_customer" type="number" name="max_price_customer" value="<%= @max_price_customer %>">
          ₽
        </label>
      </form>
    </div>

    """
  end

  defp calc_width(max_p, min_p, max_c, min_c), do: Float.round((max_c - min_c) * 100 / (max_p - min_p), 0)
  defp calc_margin(max_p, min_p, min_c), do: Float.round((min_c - min_p) * 100 / (max_p - min_p), 0)
end
