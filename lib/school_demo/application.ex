defmodule SchoolDemo.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      SchoolDemo.Repo,
      # Start the Telemetry supervisor
      SchoolDemoWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: SchoolDemo.PubSub},
      # Start the Endpoint (http/https)
      SchoolDemoWeb.Endpoint
      # Start a worker by calling: SchoolDemo.Worker.start_link(arg)
      # {SchoolDemo.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: SchoolDemo.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    SchoolDemoWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
