defmodule SchoolDemo.Repo do
  use Ecto.Repo,
    otp_app: :school_demo,
    adapter: Ecto.Adapters.Postgres
end
